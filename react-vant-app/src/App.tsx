import { Button, hooks } from 'react-vant';
import logo from './logo.svg';
import './App.css';
import { PureComponent } from 'react';

// babel官网
interface IProps {
  arr: string[]
}
interface IState {
  uname: string
}
class Home extends PureComponent<IProps, IState>{
  constructor(props: any) {
    super(props)
    this.state = {
      uname: "yl"
    }
  }
  render() {
    // console.log(this.state); 
    const { uname } = this.state
    return (
      <div>Home组件-----{uname}</div>
    )
  }
}


const HomFunc: React.FC<IProps> = (props) => {
  const { arr } = props
  return (
    <>
      {
        arr.map((item:string,index:number) => {
          return (
            <h1 key={index}>{item}</h1>
          )
        })
      }
    </>
  )
}

const App: React.FC = () => {
  const { current } = hooks.useCountDown({ time: 24 * 60 * 60 })
  console.log(current)
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Button round type="primary">欢迎使用React Vant</Button>
      </header> */}
      <Home arr={["a", "b"]} />
      <HomFunc arr={["a", "b"]} />
    </div>
  );
}

export default App;
