/* 
  1、避免代码错误
  2、写出最佳实践的代码
  3、规范变量使用方式
  4、规范代码格式
  5、更好的使用新的语法
*/
module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  parser: "@typescript-eslint/parser", // 解析器
  extends: ["plugin:@typescript-eslint/recommended", "react-app"], // 扩展
  plugins: ["@typescript-eslint", "react"], // 插件
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module",
  },
  rules: {},
};